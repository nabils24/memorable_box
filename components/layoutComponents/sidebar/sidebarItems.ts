export default [
  {
    title: "Dashboard",
    icon: "mdi-view-dashboard-outline",
    to: "/",
  },
  {
    title: "Theme",
    icon: "mdi mdi-shape-outline",
    to: "/theme",
  },
  {
    title: "Buat Invoice",
    icon: "mdi-form-dropdown",
    to: "/invoicecreate",
  },
];
